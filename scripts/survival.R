library(linseed)
library(pheatmap)
library(survival)
library(survminer)
library(dplyr)
library(RColorBrewer)

DIR = 'ACC'

OUTPUT_DIR = './img'
HEAT_DIR = 'heatmap'
ZHEAT_DIR = 'heatmap_z'
SURV_DIR = 'survplot'
HR_DIR = 'hazard'

files <- list.files(path="./R6", pattern="*.lo", full.names=TRUE, recursive=TRUE)

survival_predition <- function(dataset) {
DIR<-strsplit(basename(dataset),"_")[[1]][1]
#DIR<-'BRCA'
input_lo <- paste('./R6/',DIR,'_rnaseq.lo',sep = "")
message(paste("Loading",DIR))
load(input_lo)

lo$filterDatasetByPval(0.01)

data <- lo$exp$filtered$raw
data <- log2(data+1)

set.seed(42)
km <- kmeans(data, centers=7)

pdf_name <- paste(DIR,"_heatmap",".pdf",sep="")

data <- data[order(data.frame(cluster = km$cluster)[,1]),]
pdf(file.path(OUTPUT_DIR,HEAT_DIR,pdf_name))
pheatmap(data,show_colnames=F,show_rownames = F,legend=F,
         annotation_row = data.frame(cluster = km$cluster),
         cluster_rows = F,cluster_cols = F)
dev.off()


data <- scale(data, center = TRUE, scale = TRUE)

set.seed(42)
km <- kmeans(data, centers=7)
data <- data[order(data.frame(cluster = km$cluster)[,1]),]
pdf_name <- paste(DIR,"_zheatmap",".pdf",sep="")
pdf(file.path(OUTPUT_DIR,ZHEAT_DIR,pdf_name))
pheatmap(data, show_colnames=F, show_rownames = F,legend=F,
         annotation_row = data.frame(cluster = km$cluster),
         cluster_rows = F,cluster_cols = F)
dev.off()

clusters <- data.frame(cluster = km$cluster)

joined_data <- merge(data,clusters,by="row.names")
rownames(joined_data) <- joined_data$Row.names
joined_data$Row.names <- NULL

avg_data <- data.frame(t( joined_data %>% group_by(cluster) %>% summarise_all(funs(mean)) ))
colnames(avg_data) <- avg_data[1, ] # the first row will be the header
avg_data <- avg_data[-1, ] 

avg_data['bcr_patient_barcode'] = chartr(".","-",tolower(sapply(rownames(avg_data),function(x) gsub(paste("\\.",tail(strsplit(x, "\\.")[[1]],1),"$",sep=""),"",x))))

meta <- read.table(paste('./tcga/',DIR,'/All_CDEs.txt',sep=""), header = T, sep='\t', fill = NA)

full_ds <- merge(meta[,c('bcr_patient_barcode','vital_status','days_to_death','days_to_last_followup')],avg_data,by="bcr_patient_barcode")
rownames(full_ds) <- full_ds$Row.names
full_ds$Row.names <- NULL
full_ds$days_to_death<-as.numeric(as.character(full_ds$days_to_death))
full_ds$days_to_last_followup<-as.numeric(as.character(full_ds$days_to_last_followup))
max_days <- max(max(full_ds$days_to_death,na.rm = T),max(full_ds$days_to_last_followup,na.rm = T))

full_ds <- full_ds %>% mutate(death_date = ifelse(is.na(days_to_death), 
                                       ifelse(is.na(days_to_last_followup),max_days,days_to_last_followup), 
                                       days_to_death),
                   event = ifelse(vital_status=="dead",1,0))

full_ds$days_to_death <- NULL
full_ds$days_to_last_followup <- NULL
full_ds$vital_status <- NULL
colnames(full_ds) <- sub("([0-9])", "cluster_\\1", colnames(full_ds))
full_ds <- full_ds[full_ds$death_date>0,]

surv_object <- Surv(time = full_ds$death_date, event = full_ds$event)
fit.coxph <- coxph(surv_object ~ cluster_1 + cluster_2 + cluster_3 + cluster_4 + cluster_5 + cluster_6 + cluster_7, 
                   data = full_ds)
#summary(fit.coxph)
pdf_name <- paste(DIR,"_hazard",".pdf",sep="")
ggforest(fit.coxph, data = full_ds) %>% ggexport(filename = file.path(OUTPUT_DIR,HR_DIR,pdf_name))

cox_fit <- survfit(fit.coxph)
#summary(cox_fit)
pdf_name <- paste(DIR,"_surv",".pdf",sep="")
ggsurvplot(cox_fit, data = full_ds, pval = TRUE)  %>% ggexport(filename = file.path(OUTPUT_DIR,SURV_DIR,pdf_name))
}

survmodels <- sapply(files, survival_predition)


#survival_predition('./R6/ACC_rnaseq')
