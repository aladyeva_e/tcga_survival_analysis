library(GEOquery)
library(Biobase)
library(linseed)
library(dplyr)
library(survival)
library(survminer)
library(ggplot2)
library(maxstat)
library(data.table)

#load('./R6/LUAD_rnaseq.lo')

dataset_name <- "GSE31210"
KMEANS_DIR = 'kmeans'
HR_DIR = 'hazard'
KM_DIR = 'kmplot'
SPLIT_DIR = 'splits'
output_dir <- '/Users/xkirax/Dropbox/TCGA_SURVIVAL/img_new'

ds <- LinseedObject$new(dataset_name, topGenes=10000)

gse<-getGEO(dataset_name, GSEMatrix = TRUE, AnnotGPL = T)
gse<-gse[[1]]

pheno<-pData(phenoData(gse))


for (kmeans in 3:10) {

message(paste0("Loading kmeans =",kmeans))
all_data <- as.data.frame(scale(ds$exp$full$norm, center = TRUE, scale = TRUE))
all_data['gene']<-rownames(all_data)
meta_df<-data.frame(pheno[,c("days before death/censor:ch1","death:ch1")])
dir.create(file.path(output_dir,dataset_name), showWarnings = FALSE)
dataset_path <- file.path(output_dir,dataset_name,paste0('kmeans_',kmeans))
dir.create(dataset_path, showWarnings = FALSE)
dir.create(file.path(dataset_path,KMEANS_DIR), showWarnings = FALSE)
dir.create(file.path(dataset_path,HR_DIR), showWarnings = FALSE)
dir.create(file.path(dataset_path,KM_DIR), showWarnings = FALSE)
dir.create(file.path(dataset_path,SPLIT_DIR), showWarnings = FALSE)



#kmeans<-7
all_genes<-NULL
for (cluster in c(1:kmeans)) {
  genes<-read.table(file = paste0("./clusters/",kmeans,"_LUAD_cluster_",cluster,".txt"),sep = '\t',
                    header = T, stringsAsFactors = F)
  genes['cluster']<-cluster
  genes<-genes[,c('gene','cluster')]
  all_genes<-rbind(all_genes,genes)
}
all_data<-merge(all_data,all_genes,by='gene')

rownames(all_data)<-all_data$gene
all_data$gene<-NULL
avg_dataset <- data.frame(t( all_data %>% group_by(cluster) %>% summarise_all(funs(mean)) ))
colnames(avg_dataset) <- avg_dataset[1, ]
avg_dataset <- avg_dataset[-1, ] 

avg_dataset['bcr_patient_barcode'] <- rownames(avg_dataset)
meta_df['bcr_patient_barcode'] <- rownames(meta_df)
meta_df<-meta_df[!is.na(meta_df$days.before.death.censor.ch1),]

full_dataset <- merge(meta_df,avg_dataset,by="bcr_patient_barcode")
full_dataset['death_date'] <- as.numeric(unlist(full_dataset["days.before.death.censor.ch1"]))
full_dataset['event'] <- ifelse(full_dataset$death.ch1=="dead",1,0)
full_dataset$days.before.death.censor.ch1 <- NULL
full_dataset$death.ch1 <- NULL
colnames(full_dataset) <- sub("([0-9])", "cluster_\\1", colnames(full_dataset))
full_dataset <- full_dataset[full_dataset$death_date>0,]

clusters_list<-sapply(c(1:kmeans),function(x) paste0("cluster_",x))
surv_object <- Surv(time = full_dataset$death_date, 
                         event = full_dataset$event)
get_formula <- function(x) as.formula(paste('surv_object ~',x))
uni_models <- lapply(clusters_list, 
                          function(cluster) coxph(get_formula(cluster), data=full_dataset))
get_ggforest <- function(model) {
  cluster<-names(model$coefficients)[1]
  pdf_name <- file.path(dataset_path,HR_DIR,paste0(dataset_name,'_',cluster,'_hazard.pdf'))
  ggforest(model, data = full_dataset) %>% ggexport(filename = pdf_name)  
}
lapply(uni_models,get_ggforest)

get_cutpoints_pval <- function(model){
  cluster<-names(model$coefficients)[1]
  m_test <- maxstat.test(as.formula(paste('surv_object ~',cluster)), data=full_dataset,
                         smethod="LogRank", pmethod="Lau92", iscores=TRUE)
  m_test$p.value
}
get_cutpoints <- function(model){
  tryCatch({
    cluster<-names(model$coefficients)[1]
    surv_cutpoint(data=full_dataset[,c("death_date","event",cluster)],
                  time="death_date",
                  event="event",
                  c(cluster),minprop = 0.1)
  }, 
  error=function(e) {
    message(paste("Error in",dataset_name,"dataset",cluster,". Skipping"))
    NA
  })
}
uni_splits_pval<-lapply(uni_models,get_cutpoints_pval)
uni_splits<-lapply(uni_models,get_cutpoints)

plot_cutpoint <- function(cutpoint) {
  tryCatch({
    cluster <- rownames(cutpoint$cutpoint)
    plot(cutpoint) %>% ggexport(filename = pdf_name <- file.path(dataset_path,SPLIT_DIR,paste0(dataset_name,'_',cluster,'_split.pdf')))
  }, 
  error=function(e) {
    message(paste("Error in",dataset_name,"dataset",". Skipping"))
    NA
  })
}

lapply(uni_splits,plot_cutpoint)

uni_model<-as.matrix(uni_splits_pval)
uni_model<-data.table(uni_model)
uni_model<-cbind(clusters_list,uni_model)
colnames(uni_model)<-c('cluster','p-value')
fwrite(x = uni_model,sep = "\t",
       file = file.path(dataset_path,SPLIT_DIR,paste0(dataset_name,'_split.csv')))

get_pvalue <- function(fit) summary(fit)$coefficients[,"Pr(>|z|)"]
uni_p_vals <- lapply(uni_models,get_pvalue)

plot_km_models <- function(cutpoint) {
  tryCatch({
    cluster <- rownames(cutpoint$cutpoint)
    split_value <- round(cutpoint$cutpoint$cutpoint,3)
    dataset <- full_dataset
    dataset['cluster_split'] <- ifelse(dataset[,cluster]<=split_value,1,2)
    
    surv_object <- Surv(time = dataset$death_date,
                        event = dataset$event)
    surv_formula <- as.formula('surv_object ~ cluster_split')
    surv <- surv_fit(surv_formula, data = dataset)
    
    pdf_name <- file.path(dataset_path,KM_DIR,paste0(dataset_name,'_',cluster,'_kmplot.pdf'))
    ggsurvplot(surv, data = dataset, pval = TRUE,conf.int = TRUE,risk.table = TRUE,
               legend.title = paste(cluster,"split",split_value),
               legend.labs = c(paste("Split <=",split_value), paste("Split >",split_value)),
               tables.height = 0.33) %>% ggexport(filename = pdf_name)
  }, 
  error=function(e) {
    message(paste("Error in",dataset_name,"dataset",cluster,". Skipping"))
    NA
  })
}

lapply(uni_splits,plot_km_models)


uni_model<-as.matrix(uni_p_vals)
uni_model<-data.table(uni_model)
uni_model<-cbind(clusters_list,uni_model)
colnames(uni_model)<-c('cluster','p-value')
fwrite(x = uni_model,sep = "\t",
       file = file.path(dataset_path,HR_DIR,paste0(dataset_name,'_hr.csv')))


}

#GSE31210, GSE19188 GSE19804
gse2<-getGEO("GSE19188", GSEMatrix = TRUE, AnnotGPL = T)
gse2<-gse2[[1]]
pheno2<-pData(phenoData(gse2))
meta_df2<-data.frame(pheno2[,c("overall survival:ch1","status:ch1")])
gse3<-getGEO("GSE19804", GSEMatrix = TRUE, AnnotGPL = T)
gse3<-gse3[[1]]
pheno3<-pData(phenoData(gse3))

#GSE42127, GSE37745 and GSE30219

gse4<-getGEO("GSE42127", GSEMatrix = TRUE, AnnotGPL = T)
gse4<-gse4[[1]]
pheno4<-pData(phenoData(gse4))
meta_df4<-data.frame(pheno4[,c("overall survival months:ch1","survival status:ch1")])
gse5<-getGEO("GSE37745", GSEMatrix = TRUE, AnnotGPL = T)
gse5<-gse5[[1]]
pheno5<-pData(phenoData(gse5))
meta_df5<-data.frame(pheno5[,c("days to determined death status:ch1","dead:ch1")])
gse6<-getGEO("GSE30219", GSEMatrix = TRUE, AnnotGPL = T)
gse6<-gse6[[1]]
pheno6<-pData(phenoData(gse6))
meta_df6<-data.frame(pheno6[,c(":ch1","survival status:ch1")])

#GSE2109
gse7<-getGEO("GSE2109", GSEMatrix = TRUE, AnnotGPL = T)
gse7<-gse7[[1]]
pheno7<-pData(phenoData(gse7))