DIR=./data/tcga
OUT_DIR=./img

find $DIR -name 'mRNAseq_RSEM_normalized_log2.txt' -print0 | while IFS= read -r -d '' file_name
do
    #echo "$file_name"
    new_name=$(dirname $file_name)/$(basename $(dirname "${file_name}"))"_rnaseq.txt"
    echo "$new_name"
    cp $file_name $new_name
    Rscript run_tcga.R $new_name $OUT_DIR
done
