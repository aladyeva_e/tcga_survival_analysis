library(reshape2)
library(ggplot2)
library(dplyr)
library(gtools)

# Heatmap

mydata <- mtcars[, c(1,3,4,5,6,7)]
head(mydata)

cormat <- round(cor(mydata),2)
head(cormat)

melted_cormat <- melt(cormat)
head(melted_cormat)

ggplot(data = melted_cormat, aes(Var2, Var1, fill = value))+
  geom_tile(color = "white")+
  scale_fill_gradient2(low = "blue", high = "red", mid = "white", 
                       midpoint = 0, limit = c(-1,1), space = "Lab", 
                       name="Pearson\nCorrelation") +
  theme_minimal()+ 
  theme(axis.text.x = element_text(angle = 45, vjust = 1, 
                                   size = 12, hjust = 1))+
  coord_fixed()

#dataset 1
file_path <- './R6/ACC_rnaseq.lo'
load(file_path)
lo$filterDatasetByPval(0.01)

data1 <- lo$exp$filtered$norm
dataset_name_1<-strsplit(basename(file_path),"_")[[1]][1]


#dataset 2
file_path <- './R6/BLCA_rnaseq.lo'
load(file_path)
lo$filterDatasetByPval(0.01)

data2 <- lo$exp$filtered$norm
dataset_name_2<-strsplit(basename(file_path),"_")[[1]][1]


n<-7
clusters_list <- sapply(c(1:n),function(x) paste0("cluster_",x))

zscore_dataset1 <- scale(data1, center = TRUE, scale = TRUE)
set.seed(42)
kmeans_model1 <- kmeans(zscore_dataset1, centers=n)
kmeans_dataset1 <- zscore_dataset1[order(data.frame(cluster = kmeans_model1$cluster)[,1]),]
clusters1 <- data.frame(cluster = kmeans_model1$cluster)
kmeans_dataset1 <- merge(kmeans_dataset1,clusters1,by="row.names")
rownames(kmeans_dataset1) <- kmeans_dataset1$Row.names
kmeans_dataset1$Row.names <- NULL

zscore_dataset2 <- scale(data2, center = TRUE, scale = TRUE)
set.seed(42)
kmeans_model2 <- kmeans(zscore_dataset2, centers=n)
kmeans_dataset2 <- zscore_dataset2[order(data.frame(cluster = kmeans_model2$cluster)[,1]),]
clusters2 <- data.frame(cluster = kmeans_model2$cluster)
kmeans_dataset2 <- merge(kmeans_dataset2,clusters2,by="row.names")
rownames(kmeans_dataset2) <- kmeans_dataset2$Row.names
kmeans_dataset2$Row.names <- NULL

for (i in 1:7) {
  cross_n<-intersect(rownames(kmeans_dataset1[kmeans_dataset1$cluster==i,]),
            rownames(kmeans_dataset2[kmeans_dataset1$cluster==i,]))
  print(cross_n)
  print(length(cross_n))
}



#avg_sample1 <- data.frame(apply(kmeans_dataset1[,-which(names(kmeans_dataset1)=='cluster')], 1, median))
#avg_sample2 <- data.frame(apply(kmeans_dataset2[,-which(names(kmeans_dataset2)=='cluster')], 1, median))
avg_sample1['gene']<-rownames(kmeans_dataset1)
avg_sample2['gene']<-rownames(kmeans_dataset2)


kmeans_dataset2$gene

merge(avg_sample1[avg_sample1$cluster==1,],
      avg_sample2[avg_sample2$cluster==1,],
      by='gene',all=T)


View(avg_sample1)
