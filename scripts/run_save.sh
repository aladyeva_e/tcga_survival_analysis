DIR=./data/tcga
OUT_DIR=./R6

find $DIR -name '*_rnaseq.txt' -print0 | while IFS= read -r -d '' file_name
do
    echo "$file_name"
    Rscript ./run_save.R $file_name $OUT_DIR
done
