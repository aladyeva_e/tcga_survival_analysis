library(R6)
library(linseed)
library(survival)
library(survminer)
library(ggplot2)
library(Rtsne)
library(dplyr)
library(reshape2)
library(ggrepel)
library(forestplot)
library(maxstat)
library(data.table)
library(ggdendro)
library(grid)


SurvivalObject <- R6Class("SurvivalObject",
                         public = list(
                          dataset_path = NULL,
                          kmeans_n = NULL,
                          file_path = NULL,
                          dataset_name = NULL,
                          raw_dataset = NULL,
                          log_dataset = NULL,
                          zscore_dataset = NULL,
                          kmeans_dataset = NULL,
                          kmeans_model = NULL,
                          meta_dataset = NULL,
                          full_dataset = NULL,
                          avg_dataset = NULL,
                          clusters_list = NULL,
                          surv_object = NULL,
                          linseed  = NULL,
                          genes_clusters = NULL,
                          uni_models = NULL,
                          uni_p_vals = NULL,
                          output_dir = NULL,
                          save_img = NULL,
                          uni_splits = NULL,
                          uni_splits_pval = NULL,
                          KMEANS_DIR = 'kmeans',
                          HR_DIR = 'hazard',
                          KM_DIR = 'kmplot',
                          SPLIT_DIR = 'splits',
                          initialize = function(source_dir,
                                                output_dir='img',
                                                save_imgs = T,
                                                input_dir='.',
                                                kmeans_n = 7
                                                ) {
                            self$file_path <- file.path(input_dir,source_dir)
                            self$save_img <- save_imgs
                            self$output_dir <- output_dir
                            self$kmeans_n <- kmeans_n
                            self$dataset_name<-strsplit(basename(self$file_path),"_")[[1]][1]
                          },
                          create_dirs = function() {
                            dir.create(file.path(self$output_dir,self$dataset_name), showWarnings = FALSE)
                            self$dataset_path <- file.path(self$output_dir,self$dataset_name,paste0('kmeans_',self$kmeans_n))
                            dir.create(self$dataset_path, showWarnings = FALSE)
                            dir.create(file.path(self$dataset_path,self$KMEANS_DIR), showWarnings = FALSE)
                            dir.create(file.path(self$dataset_path,self$HR_DIR), showWarnings = FALSE)
                            dir.create(file.path(self$dataset_path,self$KM_DIR), showWarnings = FALSE)
                            dir.create(file.path(self$dataset_path,self$SPLIT_DIR), showWarnings = FALSE)
                          },
                          load_data = function() {
                            message(paste("Loading",self$dataset_name))
                            load(self$file_path)
                            self$linseed <- lo

                            self$linseed$filterDatasetByPval(0.01)
                            #self$log_dataset <- self$linseed$exp$filtered$raw
                            #self$raw_dataset <- log2(self$log_dataset+1)
                            self$raw_dataset <- self$linseed$exp$filtered$norm
                            
                          },
                          kmeans = function() {
                            n<-self$kmeans_n
                            self$clusters_list <- sapply(c(1:n),function(x) paste0("cluster_",x))
                            self$zscore_dataset <- scale(self$raw_dataset, center = TRUE, scale = TRUE)
                            set.seed(42)
                            self$kmeans_model <- kmeans(self$zscore_dataset, centers=n)
                            self$kmeans_dataset <- self$zscore_dataset[order(data.frame(cluster = self$kmeans_model$cluster)[,1]),]
                            clusters <- data.frame(cluster = self$kmeans_model$cluster)
                            self$kmeans_dataset <- merge(self$kmeans_dataset,clusters,by="row.names")
                            rownames(self$kmeans_dataset) <- self$kmeans_dataset$Row.names
                            self$kmeans_dataset$Row.names <- NULL
                            self$genes_clusters <- list()
                            for (cluster in c(1:n)) {
                              self$genes_clusters[cluster]<-list(rownames(self$kmeans_dataset[self$kmeans_dataset$cluster==cluster,]))
                            }
                            self$linseed$markers <- self$genes_clusters
                            #self$tsne_plot("raw")
                            self$tsne_plot("norm")
                            if (self$save_img){
                              ggsave(file.path(self$dataset_path,self$KMEANS_DIR,paste0(self$dataset_name,'_kmeans.pdf')))
                              }
                          },
                          
                          tsne_plot = function(error) {
                            selected <- get(error, self$linseed$exp$filtered)
                            
                            set.seed(42)
                            tsne <- Rtsne(selected, perplexity = 100, max_iter = 2000)
                            toPlot <- data.frame(
                              tSNE1=tsne$Y[, 1],
                              tSNE2=tsne$Y[, 2],
                              marker=NA,
                              row.names = rownames(selected)
                            )
                            
                            for (i in 1:length(self$genes_clusters)) {
                              toPlot[self$genes_clusters[[i]], "marker"] <- i
                            }
                            
                            
                            toPlot$marker <- as.factor(toPlot$marker)
                            ggplot(data=toPlot, aes(x=tSNE1, y=tSNE2, color=marker)) +
                              geom_point(size=0.5) + theme_bw(base_size=8) +
                              theme(legend.key.size = unit(0.1, "in"), aspect.ratio = 1) +
                              guides(color=guide_legend(title="Cluster"))
                          },
                          
                          get_avg_expression = function() {
                            self$avg_dataset <- data.frame(t( self$kmeans_dataset %>% group_by(cluster) %>% summarise_all(funs(mean)) ))
                            colnames(self$avg_dataset) <- self$avg_dataset[1, ]
                            self$avg_dataset <- self$avg_dataset[-1, ] 
                            
                            self$avg_dataset['bcr_patient_barcode'] = chartr(".","-",tolower(sapply(rownames(self$avg_dataset),function(x) gsub(paste("\\.",tail(strsplit(x, "\\.")[[1]],1),"$",sep=""),"",x))))
                          },
                          
                          load_meta = function() {
                            self$meta_dataset <- read.table(paste('./tcga/',self$dataset_name,'/All_CDEs.txt',sep=""), header = T, sep='\t', fill = NA)
                            self$full_dataset <- merge(self$meta_dataset[,c('bcr_patient_barcode','vital_status','days_to_death','days_to_last_followup')],self$avg_dataset,by="bcr_patient_barcode")
                            rownames(self$full_dataset) <- self$full_dataset$Row.names
                            self$full_dataset$Row.names <- NULL
                            self$full_dataset$days_to_death<-as.numeric(as.character(self$full_dataset$days_to_death))
                            self$full_dataset$days_to_last_followup<-as.numeric(as.character(self$full_dataset$days_to_last_followup))
                            max_days <- max(max(self$full_dataset$days_to_death,na.rm = T),max(self$full_dataset$days_to_last_followup,na.rm = T))
                            
                            self$full_dataset <- self$full_dataset %>% mutate(death_date = ifelse(is.na(days_to_death), 
                                                                              ifelse(is.na(days_to_last_followup),max_days,days_to_last_followup), 
                                                                              days_to_death),
                                                          event = ifelse(vital_status=="dead",1,0))
                            
                            self$full_dataset$days_to_death <- NULL
                            self$full_dataset$days_to_last_followup <- NULL
                            self$full_dataset$vital_status <- NULL
                            colnames(self$full_dataset) <- sub("([0-9])", "cluster_\\1", colnames(self$full_dataset))
                            self$full_dataset <- self$full_dataset[self$full_dataset$death_date>0,]
                            
                          },
                          
                          build_survival_unimodels = function() {
                            
                            self$surv_object <- Surv(time = self$full_dataset$death_date, 
                                                     event = self$full_dataset$event)
                            get_formula <- function(x) as.formula(paste('self$surv_object ~',x))
                            self$uni_models <- lapply(self$clusters_list, 
                                                      function(cluster) coxph(get_formula(cluster), data=self$full_dataset))
                            get_ggforest <- function(model) {
                              cluster<-names(model$coefficients)[1]
                              pdf_name <- file.path(self$dataset_path,self$HR_DIR,paste0(self$dataset_name,'_',cluster,'_hazard.pdf'))
                              if (self$save_img) {
                                ggforest(model, data = self$full_dataset) %>% ggexport(filename = pdf_name)  
                              }
                              else {
                                ggforest(model, data = self$full_dataset)
                              }
                              
                            }
                            lapply(self$uni_models,get_ggforest)
                            
                            get_cutpoints_pval <- function(model){
                              cluster<-names(model$coefficients)[1]
                              m_test <- maxstat.test(as.formula(paste('self$surv_object ~',cluster)), data=self$full_dataset,
                                           smethod="LogRank", pmethod="Lau92", iscores=TRUE)
                              m_test$p.value
                            }
                            get_cutpoints <- function(model){
                              tryCatch({
                              cluster<-names(model$coefficients)[1]
                              surv_cutpoint(data=self$full_dataset[,c("death_date","event",cluster)],
                                                      time="death_date",
                                                      event="event",
                                                      c(cluster),minprop = 0.1)
                              }, 
                              error=function(e) {
                                message(paste("Error in",self$dataset_name,"dataset",cluster,". Skipping"))
                                NA
                              })
                            }
                            self$uni_splits_pval<-lapply(self$uni_models,get_cutpoints_pval)
                            self$uni_splits<-lapply(self$uni_models,get_cutpoints)
                            
                            plot_cutpoint <- function(cutpoint) {
                              tryCatch({
                              cluster <- rownames(cutpoint$cutpoint)
                              plot(cutpoint) %>% ggexport(filename = pdf_name <- file.path(self$dataset_path,self$SPLIT_DIR,paste0(self$dataset_name,'_',cluster,'_split.pdf')))
                            }, 
                            error=function(e) {
                              message(paste("Error in",self$dataset_name,"dataset",". Skipping"))
                              NA
                            })
                            }
                            if (self$save_img){
                            lapply(self$uni_splits,plot_cutpoint)
                            }
                            
                            uni_model<-as.matrix(self$uni_splits_pval)
                            uni_model<-data.table(uni_model)
                            uni_model<-cbind(self$clusters_list,uni_model)
                            colnames(uni_model)<-c('cluster','p-value')
                            fwrite(x = uni_model,sep = "\t",
                                   file = file.path(self$dataset_path,self$SPLIT_DIR,paste0(self$dataset_name,'_split.csv')))
                            
                            get_pvalue <- function(fit) summary(fit)$coefficients[,"Pr(>|z|)"]
                            self$uni_p_vals <- lapply(self$uni_models,get_pvalue)
                            
                            plot_km_models <- function(cutpoint) {
                              tryCatch({
                              cluster <- rownames(cutpoint$cutpoint)
                              split_value <- round(cutpoint$cutpoint$cutpoint,3)
                              dataset <- self$full_dataset
                              dataset['cluster_split'] <- ifelse(dataset[,cluster]<=split_value,1,2)
                              
                              surv_object <- Surv(time = dataset$death_date,
                                                  event = dataset$event)
                              surv_formula <- as.formula('surv_object ~ cluster_split')
                              surv <- surv_fit(surv_formula, data = dataset)
                              
                              pdf_name <- file.path(self$dataset_path,self$KM_DIR,paste0(self$dataset_name,'_',cluster,'_kmplot.pdf'))
                              ggsurvplot(surv, data = dataset, pval = TRUE,conf.int = TRUE,risk.table = TRUE,
                                         legend.title = paste(cluster,"split",split_value),
                                         legend.labs = c(paste("Split <=",split_value), paste("Split >",split_value)),
                                         tables.height = 0.33) %>% ggexport(filename = pdf_name)
                            }, 
                            error=function(e) {
                              message(paste("Error in",self$dataset_name,"dataset",cluster,". Skipping"))
                              NA
                            })
                            }
                            if (self$save_img){
                            lapply(self$uni_splits,plot_km_models)
                            }
                            
                            uni_model<-as.matrix(self$uni_p_vals)
                            uni_model<-data.table(uni_model)
                            uni_model<-cbind(self$clusters_list,uni_model)
                            colnames(uni_model)<-c('cluster','p-value')
                            fwrite(x = uni_model,sep = "\t",
                                   file = file.path(self$dataset_path,self$HR_DIR,paste0(self$dataset_name,'_hr.csv')))
                            
                            
                          }
                         )
                         )


#print(surv_cut)
#plot(surv_cut)


run_pipeline <- function(model,kmeans_n,full_dir,save_imgs=T) {
  surv_object<-SurvivalObject$new(model,output_dir = full_dir,save_imgs=save_imgs,kmeans_n=kmeans_n)
  surv_object$create_dirs()
  surv_object$load_data()
  surv_object$kmeans()
           
  #surv_object$genes_clusters
  #surv_object$get_avg_expression()
  #surv_object$load_meta()
  #surv_object$build_survival_unimodels()
  #list(#uni_splits=surv_object$uni_splits,uni_splits_pval=surv_object$uni_splits_pval,
        #clusters_list = surv_object$clusters_list,uni_p_vals=surv_object$uni_p_vals,
  #      genes_clusters=surv_object$genes_clusters,dataset_name=surv_object$dataset_name)
  
  
  surv_ds <- surv_object$kmeans_dataset
  surv_ds["gene"]<-rownames(surv_ds)
  surv_ds<-surv_ds[,c(ncol(surv_ds),1:(ncol(surv_ds)-1))]
  for (cluster in surv_object$clusters_list) {
    cluster_num <- as.numeric(gsub("cluster_","",cluster))
    fwrite(surv_ds[surv_ds$cluster==cluster_num,1:ncol(surv_ds)-1],
           file=file.path("./clusters",paste0(kmeans_n,"_",surv_object$dataset_name,"_",cluster,".txt")),sep='\t')
  }
}


plot_pvalues <- function(models, repel = T) {
  models<-t(as.matrix(models))
  df_models<-data.frame(models)
  df_models['id'] <- rownames(df_models)
  
  melt_models<-melt(df_models, id.vars = 'id', variable.name = 'X1', value.name = 'pvalue')
  melt_models<-melt_models[order(melt_models$id,melt_models$X1),]
  melt_models['cluster'] <- substring(melt_models$X1,2)
  melt_models['id'] <- sapply(strsplit(basename(melt_models$id),"_"), '[', 1)
  melt_models<-transform(melt_models, cluster = as.numeric(cluster))
  group_number = (function(){i = 0; function() i <<- i+1 })()
  melt_models <- melt_models %>% group_by(id) %>% mutate(label = group_number()) %>% mutate( is_annotate=ifelse(-log10(pvalue)>-log10(0.05), "yes", "no"))
  
  
  
  plot <- ggplot(melt_models, aes(x=label, y=pmin(-log10(pvalue),5))) +
    
    # Show all points
    geom_jitter( aes(color=as.factor(id)), alpha=0.8, size=1.3, width=0,height=0.05) +
    #geom_point( aes(color=as.factor(id)), alpha=0.8, size=1.3) +
    #scale_color_manual(values = rep(c("grey", "skyblue"), 22 )) +
    
    # custom X axis:
    scale_x_continuous("dataset",labels=unique(melt_models$id), breaks = c(1:nrow(models)) ) +
    scale_y_continuous("-log10(pvalue) |truncated at 5|",expand = c(0, 0), limits = c(-0.1,5.5)) +
    geom_hline(yintercept=-log10(0.05), linetype="dashed") +
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank(),
      axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
    )
  
  if (repel) {
    plot <- plot + geom_label_repel( data=subset(melt_models, is_annotate=="yes"), aes(label=cluster), size=2)
  }
  plot
}


get_genes <- function(model) {
  genes_table<-all_models[c("genes_clusters"),model][[1]]
  names(genes_table)<-all_models[c("clusters_list"),model][[1]]
  a=do.call(rbind,lapply(genes_table,paste0,collapse=" "))
  data.frame(dataset=all_models[c("dataset_name"),model][[1]],
             cluster=rownames(a),genes=a,row.names = NULL)
}

get_intersect_dataset <- function(models,combos){
  
  get_intersect <-function(ds_row) {
    idx1<-as.numeric(ds_row[1])
    idx2<-as.numeric(ds_row[4])
    dataset_name1 <- ds_row[2]
    dataset_name2 <- ds_row[3]
    genes1 <- models[c('genes_clusters'),dataset_name1][[1]][[idx1]]
    genes2 <- models[c('genes_clusters'),dataset_name2][[1]][[idx2]]
    cross_n<-intersect(genes1,genes2)
    length(cross_n)
  }
  
  get_length <- function(dataset_name,idx) {
    length(models[c('genes_clusters'),dataset_name][[1]][[idx]])
  }
  
  combos_df<-combos
  
  combos_df['intersects']<-apply(combos_df, 1,get_intersect)
  combos_df['length_1']<-apply(combos_df, 1,function(x) get_length(x[2],as.numeric(x[1])))
  combos_df['length_2']<-apply(combos_df, 1,function(x) get_length(x[3],as.numeric(x[4])))
  combos_df['prc'] <- combos_df['intersects']/(combos_df['length_1']+combos_df['length_2']-combos_df['intersects'])
  
  combos_df$ds<-lapply(combos_df$dataset,function(x) strsplit(basename(as.character(x)),"_")[[1]][1])
  combos_df$ds2<-lapply(combos_df$dataset2,function(x) strsplit(basename(as.character(x)),"_")[[1]][1])
  combos_df$dataset_name<-paste0(combos_df$ds,' cluster_',combos_df$idx)
  combos_df$dataset_name2<-paste0(combos_df$ds2,' cluster_',combos_df$idx2)
  
  combos_df
}

plot_intersect <- function(intersect_ds) {
  
  ggplot(data = intersect_ds, aes(dataset_name2,dataset_name, fill = prc))+
    geom_tile(color = "white")+
    guides(fill=guide_legend(title="intersect,%"))+
    scale_fill_gradient2(low = "white", high = "red", limit = c(0,1), space = "Lab") +
    theme_minimal()+ 
    theme(axis.text.x = element_blank(),
          axis.text.y = element_blank(),
          axis.title.x = element_blank(),
          axis.title.y = element_blank()) +
    coord_fixed()
}

plot_intersect_manhattan <- function(intersects,prc_limit) {
  
  group_number = (function(){i = 0; function() i <<- i+1 })()
  intersects_new <- intersects %>% group_by(dataset) %>% mutate(label = group_number()) %>% mutate( is_annotate=ifelse((prc>prc_limit & dataset!=dataset2), "yes", "no"))
  intersects_new <- intersects_new[intersects_new$prc>0,]
  intersects_new <- intersects_new[intersects_new$dataset!=intersects_new$dataset2,]
  intersects_new['ds']<-unlist(intersects_new$ds)
  intersects_new['ds2']<-unlist(intersects_new$ds2)
  
  
  ggplot(intersects_new, aes(x=label, y=prc)) +
    
    # Show all points
    geom_jitter( aes(color=as.factor(ds)), alpha=0.8, size=1.3, width=0,height=0.05) +
    
    # custom X axis:
    scale_x_continuous("dataset",labels=unique(intersects_new$ds), breaks = c(1:length(unique(intersects_new$ds))) ) +
    scale_y_continuous("%",expand = c(0, 0), limits = c(0,1)) +
    geom_hline(yintercept=prc_limit, linetype="dashed") +
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank(),
      axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
    ) +
    geom_label_repel( data=subset(intersects_new, is_annotate=="yes"), aes(label=dataset_name), size=2)
}


get_intersect_genes <- function(models,combos){
  
  get_intersect <-function(ds_row) {
    idx1<-as.numeric(ds_row[1])
    idx2<-as.numeric(ds_row[4])
    dataset_name1 <- ds_row[2]
    dataset_name2 <- ds_row[3]
    genes1 <- models[c('genes_clusters'),dataset_name1][[1]][[idx1]]
    genes2 <- models[c('genes_clusters'),dataset_name2][[1]][[idx2]]
    cross_n<-paste(intersect(genes1,genes2),collapse = " ")
  }
  
  get_length <- function(dataset_name,idx) {
    length(models[c('genes_clusters'),dataset_name][[1]][[idx]])
  }
  
  combos['genes']<-unlist(apply(combos, 1,get_intersect))
  combos$ds<-unlist(lapply(combos$dataset,function(x) strsplit(basename(as.character(x)),"_")[[1]][1]))
  combos$ds2<-unlist(lapply(combos$dataset2,function(x) strsplit(basename(as.character(x)),"_")[[1]][1]))
  combos$dataset_name<-paste0(combos$ds,' cluster_',combos$idx)
  combos$dataset_name2<-paste0(combos$ds2,' cluster_',combos$idx2)
  combos<-combos[combos$ds!=combos$ds2,]
  combos<-combos[nchar(combos$genes)>0,]
  combos<-combos[order(-nchar(combos$genes)),]
  group_number = (function(){i = 0; function() i <<- i+1 })()
  combos <- combos %>% group_by(ds) %>% mutate(label = group_number())
  group_number = (function(){i = 0; function() i <<- i+1 })()
  combos <- combos %>% group_by(ds2) %>% mutate(label2 = group_number())
  combos[combos$label>combos$label2,c('ds','ds2','idx','idx2','genes')]
}

files <- list.files(path="./R6", pattern="*.lo", full.names=TRUE, recursive=TRUE)

for (n in 3:10) {
  MAIN_DIR <- '/Users/xkirax/Dropbox/TCGA_SURVIVAL/img_new'
  #n<-7
  all_models <- sapply(files,function(x) run_pipeline(x,n,MAIN_DIR,save_imgs = F))
  
  #csv_name <- file.path(MAIN_DIR,paste0("genes_clusters_",n,".csv"))
  #fwrite(data.table(Reduce(function(...) merge(..., all=TRUE), lapply(files,get_genes))),file = csv_name)
  
  #combos_df<-expand.grid(idx = c(1:n), dataset = files, dataset2 = files, idx2 = c(1:n))
  #intersects<-get_intersect_dataset(all_models,combos_df)
  #csv_name <- file.path(MAIN_DIR,paste0("intersect_",n,".csv"))
  #fwrite(intersects,file = csv_name)
  #plot_intersect(intersects)
  #ggsave(file.path(MAIN_DIR,paste0('intersect_heatmap_',n,'.pdf')))
  
  #plot_intersect_manhattan(intersects,0.5)
  #ggsave(file.path(MAIN_DIR,paste0('intersect_manhattan_',n,'.pdf')))
  
  #csv_name <- file.path(MAIN_DIR,paste0("intersect_genes_",n,".csv"))
  #intersects_genes<-data.table(get_intersect_genes(all_models,combos_df))
  #fwrite(intersects_genes,file = csv_name)
  
  
  #tt<-sapply(t(as.matrix(all_models[c("uni_p_vals"),])),unlist)
  #colnames(tt)<-names(all_models[c("uni_p_vals"),])
  #plot_pvalues(tt,repel=T)
  #ggsave(file.path(MAIN_DIR,paste0('cox_manhattan_desc_',n,'.pdf')))
  #plot_pvalues(tt,repel=F)
  #ggsave(file.path(MAIN_DIR,paste0('cox_manhattan_',n,'.pdf')))
  
  #tt2<-sapply(t(as.matrix(all_models[c("uni_splits_pval"),])),unlist)
  #colnames(tt2)<-names(all_models[c("uni_splits_pval"),])
  #plot_pvalues(tt2,repel=T)
  #ggsave(file.path(MAIN_DIR,paste0('km_manhattan_desc_',n,'.pdf')))
  #plot_pvalues(tt2,repel=F)
  #ggsave(file.path(MAIN_DIR,paste0('km_manhattan_',n,'.pdf')))
}

